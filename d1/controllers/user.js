const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/* 
  Check if the email already exists 
  1. use the find method to find duplicate emails 
  2. error handling, if no duplicate found, return false, else, return true 

  IMPORTANT NOTE: 
  usually if we return data in the function, it is best practice to return a boolean or data rather than a string, can edit code in the frontend for the string 
*/


module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    //result is the matches
    if (result.length > 0) {
      return true;
    } else {
      //no duplicate email found
      return false;
    }
  });
};

//USER REGISTRATION
/* 
  steps: 
  1. create a new user object using the mongoose model and the info from the request body 
  2. make sure that the password is encrypted 
  .. save the new user
*/

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    age: reqBody.age,
    gender: reqBody.gender,
    email: reqBody.email,
    // password: reqBody.password,
    password: bcrypt.hashSync(reqBody.password, 10),
    //10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run inorder to encrypt the password
    //salt round - bcrypt algorithms
    mobileNo: reqBody.mobileNo,
    isAdmin: reqBody.isAdmin,
  });

  //save
  return newUser.save().then((user, error) => {
    //if registration failed
    if (error) {
      return false;
    } else {
      //registration is successful
      return true;
    }
  });
};

/* 
  User authentication 
  1. check if the user email exists in the database 
    - if user does not exist, return false 
  2. if the user exists, compare the password provided in the login form with the password stored in the database 
  3. return a json web token if the user is successfully logged in and return false if not 
*/

module.exports.loginUser = (reqBody) => {
  //findOne() method returns the first record in the collection that matches the search criteria
  //use find one instead of find method which returns all  records that match the search criteria
  return User.findOne({ email: reqBody.email }).then((result) => {
    //user does not exist
    if (result == null) {
      return false;
    } else {
      //user exists
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      //compare sync method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database; returns a true/false value depending on the result; 2 parameters - reqbody and result

      //if the password match/result of the above code is true
      if (isPasswordCorrect) {
        //generate access token (accessToken below can be any word)
        //toObject() mongoose method converts the mongoose object into a plain js object
        return { accessToken: auth.createAccessToken(result.toObject()) }; //result from this line will become the user parameter in the auth.js file
      } else {
        //passwords do not match
        return false;
      }
    }
  });
};

/* module.exports.getProfile = (profileId) => {
  return User.find({ _id: profileId._id }, { password: 0 }).then((result) => {
    if (result == null) {
      return false;
    } else {
      return result;
    }
  });
}; */

/* module.exports.getProfile = (data) => {
  return User.findById(data.userId, { password: 0 }).then((result) => {
    return result;
  });
}; */

module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};

//Enroll user to a class
/* 
  1. Find the document in the database using the user's ID 
  2. Add the course ID to the user's enrollment array 
  3. Update the document in the mongoDB atlas database 
  */

//async await allows the course to be updated before adding the course to the user
//assures the function, will return a promise
//waiting for a certain promise that needs to be processed
//in this case there are two promises being returned the user and the course updates
module.exports.enroll = async (data) => {
  //Add the course ID in the enrollments array of the user
  let isUserUpdated = await User.findById(data.userId).then((user) => {
    //adds the courseId in the user's enrollments array
    user.enrollments.push({ courseId: data.courseId });
    //saves the updated user information in the database
    return user.save().then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });
  //add the user ID in the enrollees array of the course
  let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
    //adds the userId in the course's enrollees array
    course.enrolles.push({ userId: data.userId });
    //saves the updated course information in the database
    return course.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });
  //condition that will check if the user and course documents have been updated
  //ensures both user and course have been updated at the same time
  if (isUserUpdated && isCourseUpdated) {
    return true;
  } else {
    return false;
  }
};
