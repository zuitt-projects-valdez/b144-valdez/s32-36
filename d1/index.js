//dependencies
const express = require("express");
const mongoose = require("mongoose");
//allows us to control app's Cross Origin Resource Sharing settings
const cors = require("cors");

//Routes
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");
//server set up
const app = express();
const port = 4000;
app.use(cors()); //allows all resources/origin to access our backend application; enables all cors; not best practice for security purposes
/* 
Allowing specific domains to have access to backend 
app.use(cors(corsOptions()));
let corsOptions={
  origin: ["http://localhost:3000", "http://localhost:8000"]
} */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//defines the /api/users string to be included for all routes defined in the user route file - base url for the users; can have more than one base url - separating the users and courses routes
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

//database connection
mongoose.connect(
  "mongodb+srv://vvaldez626:admin@wdc028-course-booking.0r6u7.mongodb.net/batch144_booking_system?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console.error, "connection error"));
db.once("open", () => console.log(`Now connected to MongoDB Atlas`));

//listen
app.listen(port, () => console.log(`API is now online at port ${port}`));
